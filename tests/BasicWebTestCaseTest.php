<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BasicWebTestCaseTest extends WebTestCase
{
    // TODO: test that the server responds 200, that vue is ok, test that images are all right, test that webserver is not misconfigured ...
    public function testSomething(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Hello World');
    }
}
