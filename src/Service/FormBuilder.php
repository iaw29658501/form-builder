<?php


namespace App\Service;


use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Question;
use App\Entity\Quiz;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

class FormBuilder
{
    /**
     * @var FormFactoryInterface
     */
    private FormFactoryInterface $formFactory;
    /**
     * @var UrlGeneratorInterface
     */
    private UrlGeneratorInterface $router;
    /**
     * @var IriConverterInterface
     */
    private IriConverterInterface $iriConverter;

    /**
     * FormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     * @param UrlGeneratorInterface $router
     * @param IriConverterInterface $iriConverter
     */
    public function __construct(FormFactoryInterface $formFactory, UrlGeneratorInterface $router,IriConverterInterface $iriConverter)
    {
        $this->formFactory = $formFactory;
        $this->router= $router;
        $this->iriConverter = $iriConverter;
    }
    public function makeFormFromQuiz(Quiz $quiz): \Symfony\Component\Form\FormInterface
    {
        return $this->makeForm($this->router->generate('quiz_submit',['id'=>$quiz->getId()]), ...$quiz->getQuestions());
    }
    public function makeForm(string $uri = null, Question ...$questions): \Symfony\Component\Form\FormInterface
    {
        $form = $this->formFactory->createBuilder();
        if ($uri !== null) {
            $form->setAction($uri);
        }
        foreach ($questions as $question) {
            switch($question->getType()) {
                case 'text':
                    $form->add($question->getId(),TextType::class,[
                        'label' => $question->getLabel()
                    ]);
                     break;
                case 'single_choice':
                    $form->add($question->getId(),ChoiceType::class, [
                        'choices' => array_combine($question->getOptions(),$question->getOptions()),
                        'label' => $question->getLabel()
                    ]);
                    break;
                case 'multiple_choice':
                    $form->add($question->getId(),ChoiceType::class, [
                        'label' => $question->getLabel(),
                        'choices' => array_combine($question->getOptions(),$question->getOptions()),
                        'expanded' => true,
                        'multiple' => true
                    ]);
                    break;
            }
        }
        $form->add('Send answers', SubmitType::class, [
            'attr' => ['class' => 'bg-blue-500 text-white p-2 rounded'],
        ]);
        return $form->getForm();
    }
}