<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 * @ApiResource(
 *       collectionOperations={"get","post"},
 *       itemOperations={"get","put","patch","delete"}
 * )
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 8,
     *      max = 180,
     *      minMessage = "Your first question must be at least {{ limit }} characters long",
     *      maxMessage = "Your first question cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "example"="Are you a robot ?"
     *         }
     *     }
     * )
     * @Groups({"quiz:read"})
     */
    private $label;

    // TODO: this would not be an string but a relationship with another entity called QuestionType :P
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^(single_choice|multiple_choice|text)$/",
     *     match=true,
     *     message="The type of answer of your question showld be 'single_choice' or 'multiple_choice' or 'text' for the user type the answer"
     * )
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "example"="single_choice"
     *         }
     *     }
     * )
     * @Groups({"quiz:read"})
     */
    private $type;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *              "type":"array",
     *             "example"= { "yes","no" }
     *         }
     *     }
     * )
     * @Groups({"quiz:read"})
     */
    private $options = [];

    /**
     * @ORM\ManyToOne(targetEntity=Quiz::class, inversedBy="questions")
     */
    private ?Quiz $quiz;

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload): void
    {
        if ($this->getType() === 'text' && $this->getOptions() !== null) {
            $context->buildViolation("Questions of type 'text' should not have options, try type 'single' or 'multiple' to specify the options selection")
                ->atPath('options')
                ->addViolation();
        }
        if (  $this->getOptions() === null && ($this->getType() === 'single_choice' || $this->getType() === 'multiple_choice')  ) {
            $context->buildViolation("If the question is about choosing from options you need to give the options")
                ->atPath('options')
                ->addViolation();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }


    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

}
