<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QuizRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=QuizRepository::class)
 * @ApiResource(
 *	itemOperations={
 *		"get"={
 *			"normalization_context"={
 *				"groups"={
 *					"quiz:read"
 *				}
 *			}
 *		},"put","delete","patch"
 *	},
 *	denormalizationContext={"groups"={"quiz:write"}}
 * )
 */
class Quiz
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="quiz")
     * @Groups({"quiz:read","quiz:write"})
     */
    private Collection $questions;

    /**
     * @ORM\OneToMany(targetEntity=Submit::class, mappedBy="quiz", orphanRemoval=true)
     */
    private $submits;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->submits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setQuiz($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->removeElement($question)) {
            // set the owning side to null (unless already changed)
            if ($question->getQuiz() === $this) {
                $question->setQuiz(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Submit[]
     */
    public function getSubmits(): Collection
    {
        return $this->submits;
    }

    public function addSubmit(Submit $submit): self
    {
        if (!$this->submits->contains($submit)) {
            $this->submits[] = $submit;
            $submit->setQuiz($this);
        }

        return $this;
    }

    public function removeSubmit(Submit $submit): self
    {
        if ($this->submits->removeElement($submit)) {
            // set the owning side to null (unless already changed)
            if ($submit->getQuiz() === $this) {
                $submit->setQuiz(null);
            }
        }

        return $this;
    }
}
