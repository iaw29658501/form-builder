<?php

namespace App\Controller;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Quiz;
use App\Entity\Submit;
use App\Repository\QuestionRepository;
use App\Service\FormBuilder;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Router;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/v2/api/quiz_")
 */
class QuizController extends AbstractController
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;
    /**
     * @var FormBuilder
     */
    private FormBuilder $formBuilder;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var IriConverterInterface
     */
    private IriConverterInterface $iriConverter;


    /**
     * QuizController constructor.
     * @param ValidatorInterface $validator
     * @param FormBuilder $formBuilder
     * @param SerializerInterface $serializer
     */
    public function __construct(ValidatorInterface $validator, FormBuilder $formBuilder, SerializerInterface $serializer, IriConverterInterface $iriConverter)
    {
        $this->validator = $validator;
        $this->formBuilder = $formBuilder;
        $this->serializer = $serializer;
        $this->iriConverter = $iriConverter;
    }
    // TODO: Swagger documentation
    /**
     * @Route("submit/{id}",name="quiz_submit",methods={"POST"})
     * @param Request $request
     * @param Quiz $quiz
     * @return Response
     *
     */
    public function submit(Request $request, Quiz $quiz): Response
    {
        $form = $this->formBuilder->makeFormFromQuiz( $quiz );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $submit = new Submit();
            $submit->setDatetime(new \DateTime());
            $submit->setData($form->getData());
            $submit->setQuiz($quiz);
            $em->persist($submit);
            $em->flush();
            return $this->render('show-form.html.twig',[
                'form'=>$form->createView(),
                'id'=>$quiz->getId()
            ]);
        } else {
            return $this->render('show-form.html.twig',[
                'form'=>$form->createView(),
                'id' => $quiz->getId()
            ]);
        }
    }
    // TODO: Swagger documentation
    /**
     * @Route("generator", name="quiz_generator", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function quizGeneratorPreview(Request $request): Response
    {
        $show_submissions_link = true;
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            try {
                $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $e) {
                return $this->json('Invalid Json',400);
            }
            $request->request->replace(is_array($data) ? $data : array());
            $show_submissions_link = false;
        }
        $questions_from_db = [];
        // TODO: Validation, simple example:
        $constraints = new Assert\All([
            new Assert\Type('string')
        ]);
        $errors = $this->validator->validate(
            $questions,
            $constraints
        );
        if (count($errors) !== 0) {
            return $this->json($errors, 400);
        }
        $questions = $request->get('questions');

            foreach ($questions as $question_id) {
                //  $question = $questionRepository->find($question_id);
                $question = $this->iriConverter->getItemFromIri($question_id);
                if (!$question) {
                    return $this->json("Could not found question " . $question_id, 404);
                }
                $questions_from_db[] = $question;
            }
            $form = $this->formBuilder->makeForm(   null, ...$questions_from_db);
        // TODO: use the twig filter without the template
        return $this->render('show-form.html.twig',[
            'form'=>$form->createView(),
            'show_submissions_link' => $show_submissions_link,
            'id'=> $quiz !== null ? $quiz->getId() : null
        ]);
    }

    /**
     * @Route("generator/{id}", name="quiz_generator", methods={"GET"})
     * @param Quiz|null $quiz
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @param QuestionRepository $questionRepository
     * @return Response
     */
    public function quizGenerator(Quiz $quiz, Request $request, FormBuilder $formBuilder, QuestionRepository $questionRepository): Response
    {
        $show_submissions_link = true;
        $form = $this->formBuilder->makeFormFromQuiz($quiz);
        // TODO: use the twig filter without the template
        return $this->render('show-form.html.twig',[
            'form'=>$form->createView(),
            'show_submissions_link' => $show_submissions_link,
            'id'=> $quiz->getId()
        ]);
    }

    /**
     * @Route("submissions/{id}",name="quiz_submissions",methods={"GET"})
     * @param Request $request
     * @param Quiz $quiz
     * @return Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function submissions(Request $request, Quiz $quiz): Response
    {
        $submits = $quiz->getSubmits();
        return $this->json($this->serializer->normalize($submits,null,['groups'=>'no_quiz']));
    }
}

