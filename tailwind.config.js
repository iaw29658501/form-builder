module.exports = {
    theme: {
        extend: {},
        maxWidth: {
            '1/4': '25%',
            '1/2': '50%',
            '3/4': '75%',
        },
        maxHeight: {
          '36':'9em'
        },
        borderRadius: {
            'none': '0',
            'sm': '0.125rem',
            DEFAULT: '0.25rem',
            'md': '0.375rem',
            'lg': '0.5rem',
            'full': '9999px',
            'large': '12px',
            'xl':'1em',
            '2xl':'2em'
        }
    },
    darkMode: false,
    variants: {},
    plugins: []
}